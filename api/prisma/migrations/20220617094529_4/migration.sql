/*
  Warnings:

  - You are about to drop the `_MealToOrder` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `mealId` to the `Order` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_MealToOrder" DROP CONSTRAINT "_MealToOrder_A_fkey";

-- DropForeignKey
ALTER TABLE "_MealToOrder" DROP CONSTRAINT "_MealToOrder_B_fkey";

-- AlterTable
ALTER TABLE "Order" ADD COLUMN     "mealId" INTEGER NOT NULL,
ADD COLUMN     "quantity" INTEGER NOT NULL DEFAULT 1;

-- DropTable
DROP TABLE "_MealToOrder";

-- AddForeignKey
ALTER TABLE "Order" ADD CONSTRAINT "Order_mealId_fkey" FOREIGN KEY ("mealId") REFERENCES "Meal"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
