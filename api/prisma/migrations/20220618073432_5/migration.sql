/*
  Warnings:

  - You are about to drop the column `endDate` on the `Menu` table. All the data in the column will be lost.
  - You are about to drop the column `stardDate` on the `Menu` table. All the data in the column will be lost.
  - Added the required column `date` to the `Menu` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Menu" DROP COLUMN "endDate",
DROP COLUMN "stardDate",
ADD COLUMN     "date" TIMESTAMP(3) NOT NULL;
