import { PrismaClient } from '@prisma/client'
import { seedAddress, seedMeal, seedMenu, seedStaff, seedUser, seedOrder } from "./seeds";

const prisma = new PrismaClient()

const main = async () => {
    console.log('seeding started');
    await seedMeal();
    console.log('Meals seeded');
    await seedUser();
    console.log('Users seeded');
    await seedStaff();
    console.log('Staff seeded');
    await seedAddress();
    console.log('Addresses seeded');
    await seedMenu();
    console.log('Menu seeded');
    await seedOrder();
    console.log('Orders seeded');
    console.log('seeding done');
  }  

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
