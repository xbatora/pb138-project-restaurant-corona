import prisma from "../../src/client";

// seeds some addresses
export const seedAddress = async () => {
    await prisma.adress.create({
        data: {
            id: 1,
            city: 'Brno',
            street: 'Kotlarska',
            streetNumber: '28',
            zip: '60200',
            userId: 1,
        },
    });

    await prisma.adress.create({
        data: {
            id: 2,
            city: 'Brno',
            street: 'Koniklecova',
            streetNumber: '5',
            zip: '63400',
            userId: 2,
        },
    });
}
