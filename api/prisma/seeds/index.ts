import { seedMeal } from "./meal";
import { seedAddress } from "./address";
import { seedUser } from "./user";
import { seedStaff } from "./staff";
import { seedMenu } from "./menu";
import { seedOrder } from "./order";

export {
    seedMeal,
    seedUser,
    seedStaff,
    seedAddress,
    seedMenu,
    seedOrder,
}
