import prisma from "../../src/client";

// seeds some meals
export const seedMeal = async () => {
    await prisma.meal.create({
        data: {
            id: 1,
            name: 'smazak',  
            description: 'smazeny syr, hranolky, tatarska omacka',
            price: 110,
        },
    });

    await prisma.meal.create({
        data: {
            id: 2,
            name: 'losos',  
            description: 'peceny filet z lososa, varene brambory, bylinkova omacka',
            price: 160,
        },
    });

    await prisma.meal.create({
        data: {
            id: 3,
            name: 'panenka',  
            description: 'veprova panenka ve spenatovem luzku, americke brambory',
            price: 140,
        },
    });
}
