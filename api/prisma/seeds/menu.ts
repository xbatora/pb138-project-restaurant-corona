import prisma from "../../src/client";

// seeds a menu
export const seedMenu = async () => {
    await prisma.menu.create({
        data: {
            id: 1,
            date: '2022-06-27T00:00:00.087Z',
            createdAt: '2022-06-23T15:28:43.087Z',
        },
    });
}
