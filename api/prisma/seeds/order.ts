import prisma from "../../src/client";

// seeds some orders
export const seedOrder = async () => {
    await prisma.order.create({
        data: {
            id: 1,
            deliveryTime: '2022-06-28T12:10:00.087Z',
            open: true,
            createdAt: '2022-06-27T17:18:27.087Z',
            quantity: 1,
            userId: 1,
            mealId: 2,
        },
    });
}
