import prisma from "../../src/client";

// seeds admin staff
export const seedStaff = async () => {
    await prisma.staff.create({
        data: {
            id: 1,
            username: 'admin',
            password: 'admin',
        },
    });
}
