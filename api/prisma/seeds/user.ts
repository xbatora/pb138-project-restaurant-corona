import prisma from "../../src/client";
const bcrypt = require('bcrypt');

// seeds some users
export const seedUser = async () => {
    await prisma.user.create({
        data: {
            id: 1,
            email: 'milda@seznam.cz',
            firstName: 'Milos',
            lastName: 'Krupicka',
            password: await bcrypt.hash('heslo123', 10),
            phoneNumber: '758 648 135',
        },
    });

    await prisma.user.create({
        data: {
            id: 2,
            email: 'novotnaa@gmail.com',
            firstName: 'Alice',
            lastName: 'Novotna',
            password: await bcrypt.hash('password', 10),
            phoneNumber: '775 658 789',
        },
    });
}
