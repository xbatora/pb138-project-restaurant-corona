import prisma from '../client';
import { Request, Response } from 'express';

export const createAdress = async (req: Request, res: Response) => {
    const { city, street, streetNumber, zip } = req.body;
    const userId = req.userInfo.id;

    const adress = await prisma.adress.create({
        data:{
            city,
            street,
            streetNumber,
            zip,
            userId
        }
    });
    return res.status(201).json({
        status: 'success',
        data: adress
    });
}

export const getAdress = async (req: Request, res:Response) => {
    const adress = await prisma.adress.findFirst({
        where:{
            userId: req.userInfo.id,
        }
    });
    return res.status(200).json({
        status: 'success',
        data: adress
    });
}

export const updateAdress = async (req: Request, res:Response) => {
    const { city, street, streetNumber, zip } = req.body;
    const userId = req.userInfo.id;

    const adress = await prisma.adress.update({
        where:{
            userId
        },
        data:{
            city,
            street,
            streetNumber,
            zip
        }
    });
    return res.status(201).json({
        status: 'success',
        data: adress
    });
}

export const deleteAdress = async (req: Request, res:Response) => {
    const userId = req.userInfo.id;

    const adress = await prisma.adress.update({
        where:{
            userId
        },
        data:{
            deletedAt: new Date()
        }
    });
    return res.status(201).json({
        status: 'success',
        data: adress
    });
}