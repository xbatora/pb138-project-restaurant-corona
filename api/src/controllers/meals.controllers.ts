import prisma from '../client';
import { Request, Response } from 'express';


export const createMeal = async (req: Request, res:Response) => {
    const { name, description, price } = req.body;
    const meal = await prisma.meal.create({
        data:{
            name,
            description,
            price
        }
    });
    return res.status(201).json({
        status: 'success',
        data: meal
    });
};

export const getMealById = async (req: Request, res: Response) => {
    const meal = await prisma.meal.findUnique({
        where:{
            id: parseInt(req.params.id)
        }
    });
    return res.status(200).json({
        status: 'success',
        data: meal
    });
};

export const getMeals = async (req: Request, res: Response) => {
    const meals = await prisma.meal.findMany();
    return res.status(200).json({
        status: "success",
        data: meals
    });
}

export const updateMeal = async (req: Request, res: Response) => {
    const { name, description, price } = req.body;

    const updatedMeal = await prisma.meal.update({
        where:{
            id: parseInt(req.params.id)
        },
        data:{
            name,
            description,
            price
        }
    });
    return res.status(200).json({
        status: 'success',
        data: updatedMeal
    });
};

export const deleteMeal = async (req: Request, res: Response) => {
    const meal = await prisma.meal.update({
        where:{
            id: parseInt(req.params.id)
        },
        data:{
            deletedAt: new Date()
        }
    });
    return res.status(200).json({
        status: 'success',
        data: meal
    })
};