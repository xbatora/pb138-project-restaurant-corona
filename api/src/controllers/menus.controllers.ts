import prisma from '../client';
import { Request, Response } from 'express';

const moment = require('moment');

export const createMenu = async (req: Request, res: Response) => {
    const date = req.body.date
    
    const menu = await prisma.menu.create({
        data:{
            date: new Date(date)
        }
    });
    return res.status(201).json({
        status: 'success',
        data: menu
    });
};

export const getMenuById = async (req: Request, res: Response) => {
    const menu = await prisma.menu.findUnique({
        where:{
            id: parseInt(req.params.id)
        },
        include:{
            meals: true
        }
    });
    return res.status(200).json({
        status: 'success',
        data: menu
    });
};

export const addMealToMenu = async (req: Request, res: Response) => {
    const mealId = req.body.id

    const menu = await prisma.menu.update({
        where:{
            id: parseInt(req.params.id)
        },
        data:{
            meals: {
                connect: { id: mealId }
            }
        },
        include:{
            meals: true
        }
    });
    return res.status(200).json({
        status: 'success',
        data: menu
    })
};

export const removeMealFromMenu = async (req: Request, res: Response) => {
    const mealId = req.body.id

    const menu = await prisma.menu.update({
        where:{
            id: parseInt(req.params.id)
        },
        data:{
            meals: {
                disconnect: { id: mealId }
            }
        },
        include:{
            meals: true
        }
    });
    return res.status(200).json({
        status: 'success',
        data: menu
    })
};

export const deleteMenu = async (req: Request, res: Response) => {
    const menu = await prisma.menu.update({
        where:{
            id: parseInt(req.params.id)
        },
        data:{
            deletedAt: new Date()
        }
    });
    return res.send(200).json({
        status: 'success',
        data: menu
    })
};

export const getMenus = async (req: Request, res: Response) => {
    if(!req.query.date){
        const menus = await prisma.menu.findMany({
            include:{
                meals: true
            }
        });
        return res.status(200).json({
            status: "success",
            data: menus
        })
    }
    else{
        const date = new Date(`${req.query.date as string} UTC`);
        if(!moment(date, moment.ISO_8601, true).isValid()){
            return res.status(400).json({
                status: 'error',
                message: 'invalid date'
            });
        }
        const menus = await prisma.menu.findMany({
            where:{
                date,
            },
            include:{
                meals:true
            }
        });
        return res.status(200).json({
            status: "success",
            data: menus
        })
    }
}

