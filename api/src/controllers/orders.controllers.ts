import prisma from '../client';
import { Request, Response } from 'express';

export const getOrders = async (req: Request, res: Response) => {
    const { id } = req.userInfo;
    const orders = await prisma.order.findMany({
        where:{
            userId: id,
        }
    });
    return res.status(200).json({
        status: 'success',
        data: orders
    });
};

export const getOpenOrders = async (req: Request, res: Response) => {
    const openOrders = await prisma.order.findMany({
        where:{
            userId: req.userInfo.id,
            open: true
        }
    })
    return res.status(200).json({
        status: 'success',
        data: openOrders
    });
}

export const createOrder = async (req: Request, res: Response) => {
    const { deliveryTime, mealId, quantity } = req.body;
    const order = await prisma.order.create({
        data:{
            userId: req.userInfo.id,
            deliveryTime: new Date(deliveryTime),
            mealId,
            quantity
        }
    });
    return res.status(201).json({
        status: 'success',
        data: order
    });   
};

export const closeOrder = async (req: Request, res: Response) => {
    const closedOrder = await prisma.order.update({
        where:{
            id: parseInt(req.params.id),
        },
        data:{
            open: false
        }
    });
    return res.status(200).json({
        status: 'success',
        data: closedOrder
    });
}