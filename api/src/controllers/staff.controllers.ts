import prisma from '../client';
import { Request, Response } from 'express';

require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

export const login = async (req: Request, res: Response) => {
    const { username, password } = req.body;
    const staff = await prisma.staff.findUnique({
      where:{
        username,
      }
    });
    if(await bcrypt.compare(password, staff!.password)){
      const accessToken = jwt.sign(staff, process.env.ACCESS_TOKEN_SECRET_2)
      return res.status(200).json({
        status: 'success',
        data: { accessToken: accessToken }
      });
    } 
    else{
      return res.status(400).json({
        status: 'error',
        message: "Wrong password"
      });
    }
};

export const addAccount = async (req: Request, res: Response) => {
    const { username, password } = req.body;

    if (typeof username !== "string") {     //TODO: toto má byť asi niekde inde (dalsich 20 riadkov)
        return res.status(400).json({
            message: "Query param 'username' has to be of type string"
        });
    }
    if (typeof password !== "string") {
        return res.status(400).json({
            message: "Query param 'password' has to be of type string"
        });
    }

    const staff = await prisma.staff.findUnique({
        where:{
            username,
        }
    })
    if(staff){
        return res.status(400).json({
            status: 'error',
            message: 'username already exists'
        });
    }

    bcrypt.hash(password, 10, async (err: any, hash: any) => {
      if(err){
          console.log(req.params)
        return res.status(500).json({
          status: 'error',
          message: err
        });
      }
      const staff = await prisma.staff.create({
        data:{
          username,
          password: hash
        }
      });
      return res.status(201).json({
        status: 'success',
        data: staff
      });
    });
};

export const deleteAccount = async (req: Request, res: Response) => {
  const user = await prisma.staff.delete({
    where:{
      id: parseInt(req.params.id)
    }
  });
  return res.status(200).json({
    status: "success",
    message: "deleted"
  })
};
