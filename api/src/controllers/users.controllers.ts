import prisma from '../client';
import { Request, Response } from 'express';

require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

export const register = async (req: Request, res: Response) => {
  const { firstName, lastName, password, email } = req.body;
  bcrypt.hash(password, 10, async (err: any, hash: any) => {
    if(err){
      return res.status(500).json({
        status: 'error',
        message: err
      });
    }
    const user = await prisma.user.create({
      data:{
        firstName,
        lastName,
        email,
        password: hash
      }
    });
    return res.status(201).json({
      status: 'success',
      data: user
    });
  });
};

export const login = async (req: Request, res: Response) => {
    const { email, password } = req.body;
    const user = await prisma.user.findUnique({
      where:{
        email,
      }
    });
    if(await bcrypt.compare(password, user!.password)){
      const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
      return res.status(200).json({
        status: 'success',
        data: { accessToken: accessToken }
      });
    } 
    else{
      return res.status(400).json({
        status: 'error',
        message: "Wrong password"
      });
    }
};

export const deleteUser = async (req: Request, res: Response) => {
  const user = await prisma.user.update({
    where:{
      id: req.userInfo.id,
    },
    data:{
      deletedAt: new Date(),
    }
  });

  return res.status(200).json({
    status: 'success',
    data: {
      user: user,
      message: "account deleted"
    }
  });
};

export const changePassword = async (req: Request, res: Response) => {
  const userId = req.userInfo.id;
  const newPassword = req.body.newPassword;

  const user = await prisma.user.findUnique({
    where:{
      id: userId
    }
  });
  bcrypt.hash(newPassword, 10, async (err: any, hash: any) => {
    if(err){
      return res.status(500).json({
        status: 'error',
        message: err
      });
    }
    const user = await prisma.user.update({
      where:{
        id: userId
      },
      data:{
        password: hash
      }
    });
    return res.status(200).json({
      status: 'success',
      message: "password changed"
    });
  });
};

export const getUserInfoById = async (req: Request, res: Response) => {
  const userId = parseInt(req.params.id);

  const user = await prisma.user.findUnique({
    where:{
      id: userId
    }
  })
  return res.status(200).json({
    status: 'success',
    data: user
  });
};

export const getCurrentUserInfo = async (req: Request, res: Response) => {
  const userId = req.userInfo.id;

  const user = await prisma.user.findUnique({
    where:{
      id: userId
    }
  });
  return res.status(200).json({
    status: 'success',
    data: user
  });
};

export const updateUserInfo = async (req: Request, res: Response) => {
  const { email, phoneNumber, firstName, lastName } = req.body;

  const user = await prisma.user.update({
    where:{
      id: req.userInfo.id
    },
    data:{
      email,
      phoneNumber,
      firstName,
      lastName
    }
  });

  const newAccessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)

  return res.status(200).json({
    status: 'success',
    data: {
      user: user,
      accessToken: newAccessToken
    }
  })
};