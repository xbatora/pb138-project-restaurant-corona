import express, { Express } from 'express';
import cors from 'cors';
import userRouter from './routes/users.routes';
import mealRouter from './routes/meals.routes';
import adressRouter from './routes/adresses.routes';
import menuRouter from './routes/menus.routes';
import staffRouter from './routes/staff.routes';
import orderRouter from './routes/orders.routes';


const api: Express = express();
const port = 8080;

api.use(express.json());
api.use(cors());

api.use('/users', userRouter);
api.use('/meals', mealRouter);
api.use('/menus', menuRouter);
api.use('/orders', orderRouter);
api.use('/staff', staffRouter);
api.use('/adresses', adressRouter);

api.listen(port, () => {
    console.log(`Server is running at https://localhost:${port}`);
});
