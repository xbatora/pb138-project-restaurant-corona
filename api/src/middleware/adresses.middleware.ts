import prisma from '../client';
import { Request, Response, NextFunction } from 'express';

export const validateAdressInfo = (req: Request, res: Response, next: NextFunction) => {
    const { zip, street, streetNumber, city } = req.body;

    if(!zip || !street || !streetNumber || !city){
        res.status(400).json({
            status: 'error',
            message: 'Missing information'
        });
    }
    next();
};

export const ensureAdressExists = async (req: Request, res: Response, next: NextFunction) => {
    const adress = await prisma.adress.findFirst({
        where:{
            userId: req.userInfo.id
        }
    });
    if(!adress){
        return res.status(400).json({
            status: 'error',
            message: 'user does not have an adress'
        });
    }
    next();
};

export const ensureAdressDoesNotExist = async (req: Request, res: Response, next: NextFunction) => {
    const adress = await prisma.adress.findFirst({
        where:{
            userId: req.userInfo.id
        }
    });
    if(adress){
        return res.status(400).json({
            status: 'error',
            message: 'user already has adress'
        });
    }
    next();
};