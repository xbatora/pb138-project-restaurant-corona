import prisma from '../client';
import { Request, Response, NextFunction } from 'express';

export const validateMealInfo = (req: Request, res: Response, next: NextFunction) => {
    const { price } = req.body

    if(price && !(typeof(price) === 'number')){
        return res.status(400).json({
            status: 'error',
            message: 'invalid price'
        });
    }
    next();
};

export const ensureMealExist = async (req: Request, res: Response, next: NextFunction) => {
    const meal = await prisma.meal.findFirst({
        where: {
            AND: [
                {
                    id: parseInt(req.params.id),
                    deletedAt: null
                }
            ]
        }
    });
    if(!meal){
        return res.status(400).json({
            status: 'error',
            message: 'meal not found'
        });
    }
    next();
};

export const resolveMealInfo = async (req: Request, res: Response, next: NextFunction) => {
    const meal = await prisma.meal.findFirst({
        where: {
            AND: [
                {
                    id: parseInt(req.params.id),
                    deletedAt: null
                }
            ]
        }
    });
    if(!req.body.name){
        req.body.name = meal!.name;
    }
    if(!req.body.description){
        req.body.description = meal!.description;
    }
    if(!req.body.price){
        req.body.price = meal!.price;
    }
    next();
};