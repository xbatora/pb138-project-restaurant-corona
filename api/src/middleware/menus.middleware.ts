import prisma from '../client';
import { Request, Response, NextFunction } from 'express';

const moment = require('moment');

export const validateMenuInfo = async (req: Request, res: Response, next: NextFunction) => {
    const dateString = req.body.date
    console.log(dateString)
    if(!dateString){
        return res.status(400).json({
            status: 'error',
            message: 'missing information'
        });
    }
    if(!moment(dateString, moment.ISO_8601, true).isValid()){
        return res.status(400).json({
            status: 'error',
            message: 'invalid date'
        });
    }
    const menu = await prisma.menu.findFirst({
        where:{
            date: new Date(dateString)
        }
    });
    if(menu){
        return res.status(400).json({
            status: 'error',
            message: 'menu for the day already exists'
        });
    }
    next();
};

export const checkIfMealCanConnect = async (req: Request, res: Response, next: NextFunction) => {
    const mealId = req.body.id;
    const meal = await prisma.meal.findUnique({
        where:{
            id: mealId
        }
    });
    if(!meal){
        return res.status(400).json({
            status: "error",
            message: "wrong meal id"
        })
    }
    next();
};

export const ensureMenuExists = async (req: Request, res: Response, next: NextFunction) => {
    const menu = await prisma.menu.findUnique({
        where:{
            id: parseInt(req.params.id)
        }
    });
    if(!menu){
        res.status(400).json({
            status: 'error',
            message: 'menu not found'
        })
    }
    next();
};