import prisma from '../client';
import { Request, Response, NextFunction } from 'express';

const moment = require("moment");

export const checkOrderExsitence = async (req: Request, res:Response, next: NextFunction) => {
    const order = await prisma.order.findFirst({
        where:{
            id: parseInt(req.params.id),
            userId: req.userInfo.id
        }
    });
    if(!order){
        return res.status(400).json({
            status: 'error',
            message: 'order not found'
        });
    }
    next();
};

export const validateOrderCreation = async (req: Request, res: Response, next: NextFunction) => {
    const { deliveryTime, mealId, quantity } = req.body;

    if(!deliveryTime || !mealId){
        return res.status(400).json({
            status: 'error',
            message: 'missing information',
        });
    }
    if(!moment(deliveryTime, moment.ISO_8601, true).isValid() || moment(deliveryTime, moment.ISO_8601, true).isSameOrBefore(moment(new Date()).add(1, 'days'))){
        return res.status(400).json({
            status: 'error',
            message: 'too late to make an order'
        });
    }
    const meal = await prisma.meal.findUnique({
        where:{
            id: mealId
        }
    })
    if(!meal){
        return res.status(400).json({
            status: 'error',
            message: 'meal does not exist',
        });
    }
    if(quantity && !(typeof(quantity) === 'number')){
        return res.status(400).json({
            status: 'error',
            message: 'invalid quantity'
        });
    }
    next();
};

export const authorizeClosure = async (req: Request, res: Response, next: NextFunction) => {
    const order = await prisma.order.findFirst({
        where:{
            id: parseInt(req.params.id),
            userId: req.userInfo.id
        }
    });
    if(!order){
        return res.status(400).json({
            status: 'error',
            message: "forbidden"
        });
    }
    next();
};