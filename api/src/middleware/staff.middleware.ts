import prisma from '../client';
import { Request, Response, NextFunction } from 'express';

const jwt = require('jsonwebtoken');
require('dotenv').config();

export const authorizeStaff = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if(token == null){
        return res.status(401).json({
            status: 'error',
            message: 'Missing token'
        });
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET_2, (err: any, user: any) => {
        if(err){
            return res.status(403).json({
                status: 'error',
                message: "Forbidden"
            })
        }
        req.staffInfo = user;
        next();
    });
};

export const validateStaffLogin = async (req: Request, res: Response, next: NextFunction) => {
    const { username, password } = req.body;

    if(!username || !password){
        return res.status(400).json({
            status: 'error',
            message: 'Missing information'
        });
    }
    const staff = await prisma.staff.findUnique({
        where:{
            username,
        }
    })
    if(!staff){
        return res.status(400).json({
            status: 'error',
            message: 'username does not exist'
        });
    }
    next();
};
