import prisma from '../client';
import { Request, Response, NextFunction } from 'express';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config();

export const authorizeUser = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if(token == null){
        return res.status(401).json({
            status: 'error',
            message: 'Missing token'
        });
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err: any, user: any) => {
        if(err){
            return res.status(403).json({
                status: 'error',
                message: "Forbidden"
            })
        }
        req.userInfo = user;
    });
    next();
};

const validateEmail = (email: string) => {
    const emailRe = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRe.test(email)
}

export const validateRegistration = async (req: Request, res: Response, next: NextFunction) => {
    const { firstName, lastName, email, password, repeatPassword } = req.body;

    if(!firstName || !lastName || !email || !password || !repeatPassword){
        return res.status(400).json({
            status: 'error',
            message: "Missing information"
        });
    }
    if(!validateEmail(email)){
        return res.status(400).json({
            status: 'error',
            message: "Invalid email"
        });
    }
    const users = await prisma.user.findMany({
        where: {
          email,
        },
    })
    if(users.length > 0){
        return res.status(400).json({
            status: 'error',
            message: "Email already exists"
        });
    }
    if(password.length < 8){
        return res.status(400).json({
            status: 'error',
            message: "Password is too short"
        });
    }
    if(password !== repeatPassword){
        return res.status(400).json({
            status: 'error',
            message: 'Passwords do not match'
        });
    }
    next();
};

export const validateUserLogin = async (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body;
    if(!email || !password){
        return res.status(400).json({
            status: 'error',
            message: 'Missing information'
        });
    }
    const user = await prisma.user.findUnique({
        where:{
            email,
        }
    })
    if(!user){
        return res.status(400).json({
            status: 'error',
            message: 'email does not exist'
        });
    }
    next();
};

const validatePhoneNumber = (phoneNumber: string) => {
    const re = /^\+[0-9]{12,12}$/;
    return re.test(phoneNumber);
}

export const validateUserInfo = async (req: Request, res: Response, next: NextFunction) => {
    const userId = req.userInfo.id;
    const user = await prisma.user.findUnique({
        where:{
            id: userId
        }
    })
    if(req.body.phoneNumber && !validatePhoneNumber(req.body.phoneNumber)){
        return res.status(400).json({
            status: 'error',
            message: 'invalid phone number'
        });
    }
    if(req.body.email && !validateEmail(req.body.email)){
        return res.status(400).json({
            status: 'error',
            message: 'invalid email'
        });
    }
    next();
};

export const validateChangePasswordInfo = async (req: Request, res: Response, next: NextFunction) => {
    const { oldPassword, newPassword, repeatPassword } = req.body;
    const userId = req.userInfo.id;

    if(!oldPassword || !newPassword || !repeatPassword){
        return res.status(400).json({
            status: 'error',
            message: "Missing information"
          });
    }

    const user = await prisma.user.findUnique({
        where:{
            id: userId
        }
    });

    if(!await bcrypt.compare(oldPassword, user!.password)){
        return res.status(400).json({
          status: 'error',
          message: "Wrong password"
        });
    } 
    if(newPassword.length < 8){
        return res.status(400).json({
            status: 'error',
            message: "Password is too short"
        });
    }
    if(newPassword !== repeatPassword){
        return res.status(400).json({
            status: 'error',
            message: "Passwords do not match"
        });
    }
    next();
};