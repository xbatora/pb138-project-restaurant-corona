export default interface Staff{
    id: number,
    username: string,
    password: string
}