import express  from 'express';
import { createAdress, deleteAdress, getAdress, updateAdress } from '../controllers/adresses.controllers';
import { ensureAdressDoesNotExist, ensureAdressExists, validateAdressInfo } from '../middleware/adresses.middleware';
import { authorizeUser } from '../middleware/users.middleware';

const router = express.Router();

router.get("/", authorizeUser, getAdress);
router.post("/", authorizeUser, validateAdressInfo, ensureAdressDoesNotExist, createAdress);
router.patch("/", authorizeUser, ensureAdressExists, updateAdress);
router.delete("/:", authorizeUser, ensureAdressExists, deleteAdress);

export default router;