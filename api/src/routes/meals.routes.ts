import express  from 'express';
import { createMeal, deleteMeal, getMealById, getMeals, updateMeal } from '../controllers/meals.controllers';
import { ensureMealExist, resolveMealInfo, validateMealInfo } from '../middleware/meals.middleware';
import { authorizeStaff } from '../middleware/staff.middleware';

const router = express.Router();

router.get("/", getMeals)
router.get("/:id", ensureMealExist, getMealById);
router.post("/", authorizeStaff, validateMealInfo, createMeal);
router.patch("/:id", authorizeStaff, ensureMealExist, validateMealInfo, resolveMealInfo, updateMeal);
router.delete("/:id", authorizeStaff, ensureMealExist, deleteMeal);

export default router;