import express  from 'express';
import { addMealToMenu, createMenu, deleteMenu, getMenuById, getMenus, removeMealFromMenu } from '../controllers/menus.controllers';
import { checkIfMealCanConnect, ensureMenuExists, validateMenuInfo } from '../middleware/menus.middleware';
import { authorizeStaff } from '../middleware/staff.middleware';

const router = express.Router();

router.get("/", getMenus);
router.get("/:id", ensureMenuExists, getMenuById);
router.post("/", authorizeStaff, validateMenuInfo, createMenu);
router.patch("/:id/meals/add", authorizeStaff, ensureMenuExists, checkIfMealCanConnect, addMealToMenu);
router.patch("/:id/meals/remove", authorizeStaff, ensureMenuExists, removeMealFromMenu);
router.delete("/:id", authorizeStaff, ensureMenuExists, deleteMenu);

export default router;