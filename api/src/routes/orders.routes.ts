import express  from 'express';
import { closeOrder, createOrder, getOpenOrders, getOrders } from '../controllers/orders.controllers';
import { authorizeClosure, checkOrderExsitence, validateOrderCreation } from '../middleware/orders.middleware';
import { authorizeUser } from '../middleware/users.middleware';

const router = express.Router();

router.get("/", authorizeUser, getOrders);
router.get("/open", authorizeUser, getOpenOrders);
router.post("/", authorizeUser, validateOrderCreation, createOrder);
router.patch("/:id/close", authorizeUser, checkOrderExsitence, authorizeClosure, closeOrder);

export default router;