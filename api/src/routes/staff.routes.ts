import express  from 'express';
import { addAccount, deleteAccount, login } from '../controllers/staff.controllers';
import { validateStaffLogin } from '../middleware/staff.middleware';

const router = express.Router();

router.post("/login", validateStaffLogin, login);

//dev purpose only
router.post("/add", addAccount);
router.delete("/:id", deleteAccount);

export default router;