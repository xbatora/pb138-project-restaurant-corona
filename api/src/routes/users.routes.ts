import express  from 'express';
import { login, register, deleteUser, updateUserInfo, getCurrentUserInfo, changePassword } from '../controllers/users.controllers';
import { authorizeUser, validateChangePasswordInfo, validateRegistration, validateUserInfo, validateUserLogin } from '../middleware/users.middleware';

const router = express.Router();

router.get("/me", authorizeUser, getCurrentUserInfo);
router.patch("/me/changePassword", authorizeUser, validateChangePasswordInfo, changePassword);
router.post("/signup", validateRegistration, register);
router.post("/login", validateUserLogin, login);
router.delete("/", authorizeUser, deleteUser);
router.patch("/", authorizeUser, validateUserInfo, updateUserInfo);

export default router;