import IUser from "../../src/models/user";
import IStaff from "../../src/models/staff";

export{}

declare global {
    namespace Express {
      interface Request {
        userInfo: IUser
        staffInfo: IStaff
      }
    }
  }