import React from "react";
import { ContactInfo } from "./components/ContactInfo.tsx"
import { Footer } from "./components/Footer"
import { Menu } from "./components/Menu"
import { SWRConfig } from "swr";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import fetcher from "./models/fetcher";

export const App = () => {return (
    <SWRConfig
        value={{
            refreshInterval: 3000,
                fetcher
        }}
    >
      <Router>
          <Menu />
          <Footer />
      </Router>
    </SWRConfig>
  );
};
