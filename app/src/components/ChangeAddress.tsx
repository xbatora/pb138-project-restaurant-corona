import React, {useState} from "react";
import axios from "axios";
import {useForm} from "react-hook-form";

interface AddressForm {
    zip: string,
    street: string,
    streetNumber: string,
    city: string
}

export const ChangeAddress = () => {
    // TODO
    const userId = localStorage.getItem("user");
    const [address, setAddress] = useState({
        zip: "",
        street: "",
        streetNumber: "",
        city: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        const addressData = {
            id: userId,
            zip: address.zip,
            street: address.city,
            streetNumber: address.streetNumber,
            city: address.city,
        }
        axios
            .post("http://localhost:8080/adresses/", addressData)
            .then((response) => {
                console.log(response);
                // set the state of the user
                setAddress(response.data)
                // redirect to success page TODO

            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const handleChange = (e) => {
        const value = e.target.value;
        setAddress({
            ...address,
            [e.target.name]: value
        });
    };

    const {
        register,
        formState: { errors },
    } = useForm<AddressForm>();

    return (
      <section>
        <h1>Změnit údaje</h1>
        <form className="change-address-form" onSubmit={handleSubmit}>
            <label>PSČ</label>
            <input
                className={`text-field ${errors.zip && "text-field--error"}`}
                {...register('zip', {required: true})}
                type="number"
                name="zip"
                value={address.zip}
                onChange={handleChange}
            />
            {errors.zip && (
                <p className="change__error">Zip is required</p>
            )}

            <label>Ulica</label>
            <input
                className={`text-field ${errors.street && "text-field--error"}`}
                {...register('street', {required: true})}
                type="street"
                name="street"
                value={address.street}
                onChange={handleChange}
            />
            {errors.street && (
                <p className="change__error">Street is required</p>
            )}

            <label>Číslo</label>
            <input
                className={`text-field ${errors.streetNumber && "text-field--error"}`}
                {...register('streetNumber', {required: true})}
                type="number"
                name="streetNumber"
                value={address.streetNumber}
                onChange={handleChange}
            />
            {errors.streetNumber && (
                <p className="change__error">Street number is required</p>
            )}

            <label>Mesto</label>
            <input
                className={`text-field ${errors.city && "text-field--error"}`}
                {...register('city', {required: true})}
                type="city"
                name="city"
                value={address.city}
                onChange={handleChange}
            />
            {errors.city && (
                <p className="change__error">Quantity is required</p>
            )}

            <input className="button" type="submit" value="Uložit">
            </input>
        </form>
      </section>
    );

};
