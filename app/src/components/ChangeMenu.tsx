import React, {useEffect, useState} from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios, {AxiosRequestConfig} from "axios";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {useForm} from "react-hook-form";

interface MenuForm {
    date: Date,
    meal: string,
}

export const ChangeMenu = () => {
    const [meals, setMeals] = useState({
        data: [],
    });

    // get all meals on first load
    useEffect(() => {
        axios.get('http://localhost:8080/meals/')
            .then((response) => {
                console.log(response);
                // set the state of the user
                setMeals(response.data)
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    }, []);

    // create menu for the day
    const [menu, setMenu] = useState({
        date: "",
        meal: "",
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setMenu({
            ...menu,
            [e.target.name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const menuData = {
            date: menu.date,
            meal: menu.meal
        };
        // create menu
        axios
            .post("http://localhost:8080/menus/", menuData)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
        // add meal to menu
        axios
            .post("http://localhost:8080/menus/:1/meals/add", menuData.meal)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const {
        register,
        formState: { errors },
    } = useForm<MenuForm>();

    const [myDate, setMyDate] = useState(new Date());

    return (
        <section className="change-menu">
            <h1 className="change-menu__header">Vytvoření menu</h1>
            <form className="change-menu__form" onSubmit={handleSubmit}>
                <div className="day">
                    <DatePicker
                        selected={myDate}
                        onChange={(date) => setMyDate(date)}
                        value={myDate}
                    />
                    <select
                        className="day__meal"
                        onChange={handleChange}
                    >
                        {meals.data.map((item) => {
                            return (
                                <option value={item.name}>{item.name}</option>
                            )
                        })
                        }
                    </select>
                </div>
                <input className="button" type="submit" value="Změnit"/>
            </form>
        </section>
    );
};
