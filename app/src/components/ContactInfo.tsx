import "../styles/footer.css";
import React from "react";
import PhoneIcon from '@mui/icons-material/Phone';
import PinDropIcon from '@mui/icons-material/PinDrop';
import EmailIcon from '@mui/icons-material/Email';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import {Contact} from "./Pages/Contact";

export const ContactInfo = () => {
    return (
        <Router>
            <div className="kontakt-container narrow-centered">
                <h2>Kontakt</h2>
                <div className="kontakt-list">
                    <div className="kontakt-component">
                        <PinDropIcon className="kontakt-component__icon"/>
                        <p className="kontakt-component__name">Adresa</p>
                    </div>
                    <div className="kontakt-component">
                        <PhoneIcon className="kontakt-component__icon"/>
                        <p className="kontakt-component__name">Telefonní číslo</p>
                    </div>
                    <div className="kontakt-component">
                        <EmailIcon className="kontakt-component__icon"/>
                        <p className="kontakt-component__name">Emailová adresa</p>
                    </div>
                </div>
                <Link className="button button__contact button--inverted" to="/contact">Kontaktujte nás</Link>


            </div>
            <Switch>
                <Route path="/contact" strict>
                    Kontaktujte nás
                </Route>
            </Switch>
        </Router>
    );
};