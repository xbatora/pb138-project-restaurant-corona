import "../styles/footer.css";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Staff} from "./Staff";
import {ContactInfo} from "./ContactInfo";

export const Footer = () => {
    return (
        <div className="footer-container">
            <ContactInfo />
            <p className="footer-container__copyright">© Copyright 2022 - All Rights Reserved. Created by ...</p>
            <div className="footer__item">
                <Link to="/administration">Administrace</Link>
            </div>
        </div>
    );
};
