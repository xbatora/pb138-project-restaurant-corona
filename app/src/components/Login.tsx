import React, {useEffect} from "react";
import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "../styles/registration.css";
import axios from "axios";


interface IFormInput {
    email: string;
    password: string;
}

export const Login = () => {
    const [data, setData] = useState({
        email: "",
        password: "",
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const userData = {
            email: data.email,
            password: data.password
        };
        axios
            .post("http://localhost:8080/users/login", userData)
            .then((response) => {
                console.log(response);
                // set the state of the user
                setData(response.data)
                // save to local storage
                localStorage.setItem('user', response.data.email)
                // redirect to success page
                window.location.replace("/loginsuccess")
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const {
        register,
        formState: { errors },
    } = useForm<IFormInput>();

    return (
        <section className="login-container">
            <h2 className="login__header">Login</h2>
            <form className="login-form" onSubmit={handleSubmit}>
                <label>Email:</label>
                <input
                    className={`text-field ${errors.email && "text-field--error"}`}
                    {...register('email', {required: true})}
                    type="email"
                    name="email"
                    value={data.email}
                    onChange={handleChange}
                />
                {errors.email && (
                    <p className="login__error">Email is required</p>
                )}

                <label>Password:</label>
                <input
                    className={`text-field ${errors.password && "text-field--error"}`}
                    {...register("password", { required: true})}
                    type="password"
                    name="password"
                    value={data.password}
                    onChange={handleChange}
                />
                {errors.password && (
                    <p className="login__error">Password is required</p>
                )}

                <input className="button" type="submit" value="Přihlásit">
                </input>
            </form>
        </section>
    );
};
