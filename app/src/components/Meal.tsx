import { Link } from 'react-router-dom';
import useSWR from "swr";
import fetcher from "../models/fetcher";
import menu from "../../../api/src/models/menu";
import React, {useState} from "react";
import axios, {AxiosRequestConfig} from "axios";

export interface MealProps {
    id: string
}

export const Meal = ({ id }: MealProps) => {
    const [data, setData] = useState({
        name: "",
        price: ""
    });

    const menuData: AxiosRequestConfig = {
        data: {
            id: id,
        }
    }
    const currentMeal = {
        name: data.name,
        price: data.price
    };
    axios.get('http://localhost:8080/meals/:' + {id})
        .then((response) => {
            console.log(response);
            // set the state of the user
            setData(response.data)
        });

    return (
        <div className="meal">
            <div className="meal__info">
                <div className="meal__name">
                    <div>{currentMeal.name}</div>
                </div>
                <div className="meal__price">
                    <div>{currentMeal.price}</div>
                </div>
            </div>
        </div>
    );
};
