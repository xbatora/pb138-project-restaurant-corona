import React, {useState, Component, useEffect} from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios from "axios";
import {useForm} from "react-hook-form";

interface MealForm {
    name: string,
    description: string,
    price: string,
}

export const Meals = () => {
    const [data, setData] = useState({
        data: [],
    });

    // load the initial meals
    useEffect(() => {
        axios.get('http://localhost:8080/meals/')
            .then((response) => {
                console.log(response);
                // set the state of the user
                setData(response.data)
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    }, []);

    const [newMeal, setNewMeal] = useState({
        name: "",
        description: "",
        price: "",
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setNewMeal({
            ...newMeal,
            [e.target.name]: value
        });
    };

    // add new meal
    const handleSubmit = (e) => {
        e.preventDefault();
        const mealData = {
            name: newMeal.name,
            description: newMeal.description,
            price: newMeal.price,
        };
        axios
            .post("http://localhost:8080/meals/", mealData)
            .then((response) => {
                console.log(response);
                // set the state of the user
                setNewMeal(response.data)
                // redirect to success page
                window.location.replace("/meals")
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const {
        register,
        formState: { errors },
    } = useForm<MealForm>();

    return (
        <div className="footer-container" >
            <h1>Meals</h1>
            {data.data.map((item) => {
                return (
                    <div className="meal">
                        <div className="meal__name">
                            {item.name}
                        </div>
                        <div className="meal__desciption">
                            {item.description}
                        </div>
                        <div className="meal__price">
                            {item.price}
                        </div>
                    </div>
                )
            })
            }

            <h2>Přidat jídlo</h2>
            <form className="add-meal-form" onSubmit={handleSubmit}>
                <label>Název:</label>
                <input
                    className={`text-field ${errors.name && "text-field--error"}`}
                    {...register('name', {required: true})}
                    type="name"
                    name="name"
                    value={newMeal.name}
                    onChange={handleChange}
                />
                {errors.name && (
                    <p className="login__error">Name is required</p>
                )}

                <label>Opis:</label>
                <input
                    className={`text-field ${errors.description && "text-field--error"}`}
                    {...register('description', {required: true})}
                    type="description"
                    name="description"
                    value={newMeal.description}
                    onChange={handleChange}
                />
                {errors.description && (
                    <p className="login__error">Description is required</p>
                )}

                <label>Cena:</label>
                <input
                    className={`text-field ${errors.price && "text-field--error"}`}
                    {...register('price', {required: true})}
                    type="number"
                    name="price"
                    value={newMeal.price}
                    onChange={handleChange}
                />
                {errors.price && (
                    <p className="login__error">Price is required</p>
                )}
                <input className="button" type="submit" value="Přidat" />
            </form>
        </div>
    );
};
