import "../styles/menu.css";
import React from "react";
import MenuIcon from '@mui/icons-material/Menu';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {About} from "./Pages/About";
import { Home } from "./Pages/Home"
import { Contact } from "./Pages/Contact";
import {WeeklyMenu } from "./Pages/WeeklyMenu";
import { Order } from "./Pages/Order";
import {Register} from "./Register";
import {User} from "./User";
import {Login} from "./Login";
import {Staff} from "./Staff";
import {Navbar} from "./Navbar";
import {ChangeAddress} from "./ChangeAddress";
import {Meals} from "./Meals";
import {ChangeMenu} from "./ChangeMenu";



export const Menu = () => {
    return (
        <Router>
            <Navbar>
                <Link to="/">Úvod</Link>
                <Link to="/about">O nás</Link>
                <Link to="/weeklymenu">Týdenní menu</Link>
                <Link to="/order">Objednat</Link>
                <Link to="/user">User</Link>
                <Link to="/contact">Kontakt</Link>
            </Navbar>
            <Switch>
                <Route path="/" exact>
                    <Home />
                </Route>
                <Route path="/about" exact>
                    <About />
                </Route>
                <Route path="/weeklymenu" exact>
                    <WeeklyMenu />
                </Route>
                <Route path="/order" exact>
                    <Order />
                </Route>
                <Route path="/user" exact>
                    <User />
                </Route>
                <Route path="/contact" exact>
                    <Contact />
                </Route>
                <Route path="/register" exact>
                    <Register />
                </Route>
                <Route path="/login" exact>
                    <Login />
                </Route>
                <Route path="/loginsuccess">
                    <h1>Login success</h1>
                </Route>
                <Route path="/registrationsuccess">
                    <h1>Registration success</h1>
                </Route>
                <Route path="/administration">
                    <Staff />
                </Route>
                <Route path="/address" exact>
                    <ChangeAddress />
                </Route>
                <Route path="/meals" exact>
                    <Meals />
                </Route>
                <Route path="/changeMenu" exact>
                    <ChangeMenu />
                </Route>
            </Switch>
        </Router>
    );
};
