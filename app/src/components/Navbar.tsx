import React from "react";

import "../styles/navbar.css";

type Props = {
    children?: React.ReactNode;
};



export const Navbar: React.FC<Props> = ({
                                       children,
                                   }) => (
<div className="navbar-container">
    <header className="menu-container narrow-centered">
        <div className="menu__logo">Ø</div>
        <label
            className="hamburger-icon"
            aria-label="Open navigation menu"
            htmlFor="menu-toggle"
        >&#9776;</label>
        <input type="checkbox" id="menu-toggle"/>
        <nav className="navigation-menu">
            <ul className="navigation-list">
                {children}
            </ul>
        </nav>
    </header>
</div>
)

