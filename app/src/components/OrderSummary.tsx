import React, {useState} from "react";
import axios, {AxiosRequestConfig} from "axios";
import {useForm} from "react-hook-form";

export interface OrderProps {
    id: string
}

interface OrderForm {
    deliveryTime: string,
    mealId: string,
    quantity: string
}

export const OrderSummary = ({ id}: OrderProps) => {
    let mealCount = 1;

    const [meal, setMeal] = useState({
        name: "",
        price: "",
    });

    // load the meal by id
    axios
        .get("http://localhost:8080/meals/:" + {id})
        .then((response) => {
            console.log(response);
            // set the state of the user
            setMeal(response.data)
        })
        .catch((error) => {
            if (error.response) {
                console.log(error.response);
                console.log("server responded");
            } else if (error.request) {
                console.log("network error");
            } else {
                console.log(error);
            }
        });

    // place order with meal and mealCount
    const [order, setOrder] = useState({
        deliveryTime: "",
        mealId: "",
        quantity: ""
    });

    const placeOrder = (e) => {
        e.preventDefault();
        const orderData = {
            deliveryTime: order.deliveryTime,
            mealId: order.mealId,
            quantity: order.quantity
        };
        axios
            .post("http://localhost:8080/orders/", orderData)
            .then((response) => {
                console.log(response);
                // set the state of the user
                setOrder(response.data)
                // redirect to success page TODO
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const handleChange = (e) => {
        const value = e.target.value;
        setOrder({
            ...order,
            [e.target.name]: value
        });
    };

    const {
        register,
        formState: { errors },
    } = useForm<OrderForm>();

    return (
        <div className="order-summary">
            <form className="order-form" onSubmit={placeOrder}>
                <p>{meal.name}</p>

                <label>Počet</label>
                <input
                    className={`text-field ${errors.quantity && "text-field--error"}`}
                    {...register('quantity', {required: true})}
                    type="number"
                    name="quantity"
                    value={order.quantity}
                    onChange={handleChange}
                />
                {errors.quantity && (
                    <p className="order__error">Quantity is required</p>
                )}

                <label>Čas dodania</label>
                <input
                    className={`text-field ${errors.deliveryTime && "text-field--error"}`}
                    {...register('deliveryTime', {required: true})}
                    type="deliveryTime"
                    name="deliveryTime"
                    value={order.deliveryTime}
                    onChange={handleChange}
      k          />
                {errors.deliveryTime && (
                    <p className="order__error">Quantity is required</p>
                )}

                <input className="button" type="submit" value="Objednat">
                </input>
            </form>
        </div>
    );
};
