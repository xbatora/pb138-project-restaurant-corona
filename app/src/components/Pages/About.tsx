import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {Order} from "./Order";

export const About = () => {
    return (
        <Router>
            <div className="about-container narrow-centered">
                <img className="about__image" src={'https://images.pexels.com/photos/3186654/pexels-photo-3186654.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'} alt="Obrázek O nás"/>
                <h2 className="about__header">O nás</h2>
                <p className="about__description">Vaříme poctivou českou kuchyni a denně pro vás připravujeme polévku a výběr ze čtyř jídel + moučník. Jedno z jídel je vždy bezlepkové. Dále máme ve stálé nabídce šest druhů salátů a dvě XXL menu.

                    Zajišťujeme rauty, občerstvení na seminářích, na podnikových večírcích a dalších akcích.

                    Naši jídelnu můžete osobně navštívit v době od 11:00 do 14:00 hodin.
                </p>
                <Link className="button button__order" to="/order">Objednat oběd</Link>
            </div>
            <Switch>
                <Route path="/order" strict>
                    <Order />
                </Route>
            </Switch>
        </Router>
    );
};
