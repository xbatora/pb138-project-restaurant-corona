import "../../styles/home.css";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { WeeklyMenu } from "./WeeklyMenu";
import { Order } from "./Order"

export const Home = () => {
  return (
      <Router>
          <div className="home-container narrow-centered">
              <img className="main-image" src={'https://images.pexels.com/photos/3184192/pexels-photo-3184192.jpeg?auto=compress&cs=tinysrgb&w=1600'} alt="Restaurace Korona"/>
              <h1>Restaurace Korona</h1>
              <div className="cards">
                  <div className="card card__about">
                      <div className="card__text">
                          <h2>O nás</h2>
                          <p>Vážení hosté, připravili jsme pro Vás nabídku jídel do zázemí Vašich domovů. Nabízíme možnost si objednat u nás jídla s rozvozem či k vyzvednutí ke konzumaci v pohodlí vašich domovů či kanceláří.</p>
                          <Link className="button button--inverted button__order" to="/order">Objednat oběd</Link>
                      </div>
                      <div className="card__image">
                          <img src={'https://www.coolinari.sk/wp-content/uploads/2022/05/IMG_9129.jpg'} alt="Obrázek O nás"/>
                      </div>
                  </div>

                  <div className="card card__menu card__inverted">
                      <div className="card__text">
                          <h2 className="card__header">Týdenní menu</h2>
                          <p className="card__description">Každý týden máme pro Vás připravený výběr hlavních jídel.</p>
                          <Link className="button button--inverted button__weekly-menu" to="/weeklymenu">Více</Link>
                      </div>
                      <div className="card__image">
                          <img src={'https://d50-a.sdn.cz/d_50/c_img_QN_R/JOTEh/kulajda.jpeg?fl=cro,0,0,2000,1125%7Cres,1200,,1%7Cwebp,75'} alt="Obrázek Týdenní menu"/>
                      </div>
                  </div>
              </div>

          </div>
          <Switch>
              <Route path="/order" strict>
                  <Order />
              </Route>
              <Route path="/weeklymenu" strict>
                  <WeeklyMenu />
              </Route>
          </Switch>
      </Router>
  );
};
