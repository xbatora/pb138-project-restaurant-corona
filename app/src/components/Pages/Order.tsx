import React, {useEffect, useState} from "react";
import {User} from "../User";
import {WeeklyMenu} from "./WeeklyMenu";
import axios, {AxiosRequestConfig} from "axios";
import {useForm} from "react-hook-form";
import menu from "../../../../api/src/models/menu";
import useSWR from 'swr'
import fetcher from "../../models/fetcher"
import { Meal } from "../Meal";
import {OrderSummary} from "../OrderSummary";

export const Order = () => {
    const loggedInUser = localStorage.getItem("user");
    const id = localStorage.getItem("menu")
    let mealId = "";

    const [data, setData] = useState({
        meals: [],
    });

    const menuData: AxiosRequestConfig = {
        data: {
            date: new Date()
        }
    }

    const currentMenu = {
        meals: data.meals
    };

    // get menu for current day
    axios.get('http://localhost:8080/menus/', menuData)
        .then((response) => {
            console.log(response);
            // set the state of the user
            setData(response.data)
        });

    if (loggedInUser) {
        if (currentMenu.meals) {
            return (
                <div className="order-container">
                    <h1 className="order__header">Objednat oběd</h1>
                    {currentMenu.meals.map((item) => {
                        mealId = item.id;
                        return (
                            <div>
                                <Meal key={item.id} {...item} />
                                <input className="button"
                                       type="button"
                                       id="add"
                                       value={"Objednat"}
                                       onClick={() => <OrderSummary id={item.id} />}/>
                            </div>
                            )
                        })
                    }
                </div>
            );
        } else {
            return (
                <p>No menu for this week</p>
            )
        }

    }
    else {
        return (
            <User />
        );
    }
};
