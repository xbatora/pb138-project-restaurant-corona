import React, {useState} from "react";
import axios, {AxiosRequestConfig} from "axios";

export const WeeklyMenu = () => {
    const [data, setData] = useState({
        data: [],
    });

    const menuData: AxiosRequestConfig = {
        data: {
            date: new Date()
        }
    }

    // get menu for current day
    axios.get('http://localhost:8080/meals/')
        .then((response) => {
            console.log(response);
            // set the state of the user
            setData(response.data)
        })
        .catch((error) => {
            if (error.response) {
                console.log(error.response);
                console.log("server responded");
            } else if (error.request) {
                console.log("network error");
            } else {
                console.log(error);
            }
        });

    if (data.data) {
        return (

            <section className="weekly-menu">
                <h1 className="weekly-menu__header">Týdenní menu</h1>
                {data.data.map((item) => {
                    return (
                        <div className="meal">
                            <div className="meal__name">
                                {item.description}
                            </div>
                            <div className="meal__price">
                                {item.price}
                            </div>
                        </div>
                    )
                })
                }
            </section>
        );
    }
};
