import React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import "../styles/registration.css";
import axios from "axios";

interface IFormInput {
    firstName: string;
    lastName: string;
    street: string;
    streetNumber: string;
    city: string;
    zip: string;
    phone: string;
    email: string;
    password: string;
    repeatPassword: string
}

export const Register = () => {
    const [data, setData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        repeatPassword: ""
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const userData = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: data.password,
            repeatPassword: data.repeatPassword
        };
        axios
            .post("http://localhost:8080/users/signup", userData)
            .then((response) => {
                console.log(response);
                window.location.replace("/login")
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const {
        register,
        formState: { errors },
    } = useForm<IFormInput>();

    return (
        <section className="register-container">
            <h2 className="register__header">Register</h2>
            <form className="form" onSubmit={handleSubmit}>
                <label>First name:</label>
                <input
                    className={`text-field ${errors.firstName && "text-field--error"}`}
                    {...register('firstName', {required: true})}
                    type="firstName"
                    name="firstName"
                    value={data.firstName}
                    onChange={handleChange}
                />
                {errors.firstName && (
                    <p className="registration__error">First name is required</p>
                )}

                <label>Last name:</label>
                <input
                    className={`text-field ${errors.lastName && "text-field--error"}`}
                    {...register("lastName", { required: true })}
                    type="lastName"
                    name="lastName"
                    value={data.lastName}
                    onChange={handleChange}
                />
                {errors.lastName && (
                    <p className="registration__error">Last name is required</p>
                )}

                <label>Email:</label>
                <input
                    className={`text-field ${errors.email && "text-field--error"}`}
                    {...register("email", { required: true })}
                    type="email"
                    name="email"
                    value={data.email}
                    onChange={handleChange}
                />
                {errors.email && (
                    <p className="registration__error">Email is required</p>
                )}

                <label>Password:</label>
                <input type="password"
                    className={`text-field ${errors.password && "text-field--error"}`}
                    {...register("password", { required: true })}
                   name="password"
                   value={data.password}
                   onChange={handleChange}
                />
                {errors.password && (
                    <p className="registration__error">Password is required</p>
                )}

                <label>Confirm password:</label>
                <input type="password"
                   className={`text-field ${errors.repeatPassword && "text-field--error"}`}
                   {...register("repeatPassword", { required: true })}
                   name="repeatPassword"
                   value={data.repeatPassword}
                   onChange={handleChange}
                />
                {errors.repeatPassword && (
                    <p className="registration__error">Password confirmation is required</p>
                )}

                <input className="button" type="submit" value="Registrovat"/>
            </form>
        </section>
    );
};
