import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {Meals} from "./Meals";
import {ChangeMenu} from "./ChangeMenu";
import {StaffLogin} from "./StaffLogin";

export const Staff = () => {
    if (!localStorage.getItem("staff")) {
        return (
            <StaffLogin />
            )
    }
    return (
        <Router>
            <div className="staff-container">
                <h1 className="staff__header">Staff</h1>
                <Link to="/meals">Jídla</Link>
                <Link to="/changeMenu">Upravit menu</Link>
            </div>
        </Router>
    );
};
