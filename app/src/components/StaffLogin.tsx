import React, {useState} from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Meals } from "./Meals"
import { ChangeMenu } from "./ChangeMenu"
import axios from "axios";
import {useForm} from "react-hook-form";

interface IFormInput {
    username: string;
    password: string;
}

export const StaffLogin = () => {

    // create Staff user
    const createStaff = () => {
        const staffData = {
            username: "staff",
            password: "staffpassword",
        };
        axios
            .post("http://localhost:8080/staff/add", staffData)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };
    // TODO
    //createStaff();

    const [data, setData] = useState({
        username: "",
        password: "",
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const staffData = {
            username: data.username,
            password: data.password,
        };
        axios
            .post("http://localhost:8080/staff/login", staffData)
            .then((response) => {
                console.log(response);
                // set the state of the user
                setData(response.data)
                // save to local storage
                localStorage.setItem('staff', response.data.username)
                // redirect to success page
                window.location.replace("/administration")
            })
            .catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log("server responded");
                } else if (error.request) {
                    console.log("network error");
                } else {
                    console.log(error);
                }
            });
    };

    const {
        register,
        formState: { errors },
    } = useForm<IFormInput>();

    return (
        <Router>
            <div className="staff-login">
                <form className="form" onSubmit={handleSubmit}>
                    <label>Username:</label>
                    <input
                        className={`text-field ${errors.username && "text-field--error"}`}
                        {...register('username', {required: true})}
                        type="username"
                        name="username"
                        value={data.username}
                        onChange={handleChange}
                    />
                    {errors.username && (
                        <p className="login__error">Email is required</p>
                    )}

                    <label>Password:</label>
                    <input
                        className={`text-field ${errors.password && "text-field--error"}`}
                        {...register("password", { required: true})}
                        type="password"
                        name="password"
                        value={data.password}
                        onChange={handleChange}
                    />
                    {errors.password && (
                        <p className="login__error">Password is required</p>
                    )}

                    <input className="button" type="submit" value="Přihlásit">
                    </input>
                </form>

                <h1 className="staff__header">Staff</h1>
                <Link to="/meals">Jídla</Link>
                <Link to="/changeMenu">Upravit menu</Link>

                <Switch>
                    <Route path="/meals" exact>
                        <Meals />
                    </Route>
                    <Route path="/changeMenu" exact>
                        <ChangeMenu />
                    </Route>
                </Switch>
            </div>
        </Router>

    );
};
