import React, {useEffect, useState} from "react";
import { BrowserRouter as Router, Switch, Route, Link, useParams } from 'react-router-dom';
import { Register } from "./Register";
import {Login} from "./Login";
import axios from "axios";
import {ChangeAddress} from "./ChangeAddress";

export const User = () => {
    const [items, setItems] = useState([]);

    /*
    axios.get("http://localhost:8080/users/me")
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            if (error.response) {
                console.log(error.response);
                console.log("server responded");
            } else {
                console.log(error);
            }
        });

     */

    const loggedUser = localStorage.getItem('user');
    if (false) {
        return (
          <Router>
              <div className="user-container">
                  <h2 className="user__header">User</h2>
                  <input className="button"
                         type="button"
                         id="logout"
                         value="Odhlásit"
                        />

                  <Link to="/address">Změnit adresu</Link>

                  <Switch>

                  </Switch>
              </div>
          </Router>
        );
    } else {
        return (
            <Router>
                <div className="user-container">
                    <h2 className="user__header">User</h2>
                    <Link to="/register">Registrovat</Link>
                    <Link to="/login">Přihlásit</Link>

                    <Switch>
                        <Route path="/register" exact>
                            <Register />
                        </Route>
                        <Route path="/login" exact>
                            <Login />
                        </Route>
                    </Switch>

                </div>
            </Router>
        );
    }
};
