export const accommodations = [
    {
        name: 'Å Auge - River Eye - Treehouse',
        description:
            'Experience being in the trees, falling asleep under the stars and waking up to the sound of birdsong in this spectacular treehouse. If you are looking for an intimate, simple and restful retreat, look no further.',
        location: 'Tinn, Telemark, Nórsko',
        mainPhoto: 'https://images.unsplash.com/photo-1550355191-aa8a80b41353?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8´',
        // dateAdded: new Date('2021-03-12').toISOString()
    },
    {
        name: 'Aska Modern Cabin',
        description:
            'Aska is a part of Hlíð Ferðaþjónusta is a complex of accomodations on the northern shore of lake Mývatn, right beneath the airport. We are situated about one kilometer away from the lakeside, in the middle of a 300 years old lava field and with a fantastic view over the lake.',
        location: 'Reykjahlíð, Island',
        mainPhoto: 'https://images.unsplash.com/photo-1579033462043-0f11a7862f7d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8',
        // dateAdded: new Date('2021-01-12').toISOString()
    },
    {
        name: 'Glass cottage with Hot tub "Blár"',
        description:
            'We are located on a lava desert in the south of Iceland. 5 minutes from the small town of Hella, close to all the popular attractions that southern Iceland has to offer, but also in a secret and secluded location.',
        location: 'Hella, Island',
        mainPhoto: 'https://images.unsplash.com/photo-1590725140246-20acdee442be?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8',
        // dateAdded: new Date('2021-01-05').toISOString()
    },
    {
        name: 'The Mud House',
        description:
            'Beautiful mud brick, earth roof open plan small house. Set in an abundant organic paradise of birds and orchards surrounded by regenerating bush within walking distance to the water in peaceful Pigeon Bay. ',
        location: 'Pigeon Bay, Canterbury, Nový Zéland',
        mainPhoto: 'https://images.unsplash.com/photo-1619292585355-ab0e3fd509fe?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1',
        // dateAdded: new Date('2020-01-05').toISOString()
    },
    {
        name: 'Casa Tata 4',
        description:
            'These gorgeous 9 cabins right in front of the sea are the perfect spot if you want to enjoy Chacahua’s captivating nature and still have a clean, quiet, luxurious and tasteful accommodation.',
        location: 'Laguna de Chacahua, Oaxaca, Mexiko',
        mainPhoto: 'https://images.unsplash.com/photo-1590725121839-892b458a74fe?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8',
        // dateAdded: new Date('2020-02-05').toISOString()
    },
]
