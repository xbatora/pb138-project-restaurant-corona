function fetcher(url: string, header: Headers) {
  return fetch(`http://localhost:8080/${url}`, {
    headers: header,
  }).then((response) => response.json());
}

export default fetcher;
