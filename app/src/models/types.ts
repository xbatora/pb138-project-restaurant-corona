export interface animal {
  id: string,
  name: string,
  description: string,
  age: number,
  sex: boolean,
  type: string, // ENUM
  picture: string,
  addedAt: Date;
}

export interface user {
  firstName: string;
  lastName: string;
  street: string;
  streetNumber: string;
  city: string;
  zip: string;
  phone: string;
  email: string;
  password: string;
  confirmPassword: string
}
