import React from 'react';
import { AboutUs } from './AboutUs';
import { ComponentMeta, ComponentStory } from '@storybook/react';

export default {
    title: 'AirBnB/About us',
    component: AboutUs,
} as ComponentMeta<typeof AboutUs>;


const Template: ComponentStory<typeof AboutUs> = () => <AboutUs />;

export const Page = Template.bind({});