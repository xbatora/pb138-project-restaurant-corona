import React from 'react';
import { AccommodationItem, AccommodationItemProps } from './AccommodationItem';
import { accommodations } from '../data/accommodations';
import { photos } from '../data/restaurace'

export const AboutUs = () => {
    return (
        <div className="ui main container text app">
            <h1>O nás</h1>
            <p>Vaříme poctivou českou kuchyni a denně pro vás připravujeme polévku a výběr ze čtyř jídel + moučník. Jedno z jídel je vždy bezlepkové. Dále máme ve stálé nabídce šest druhů salátů a dvě XXL menu.

                Zajišťujeme rauty, občerstvení na seminářích, na podnikových večírcích a dalších akcích.

                Naši jídelnu můžete osobně navštívit v době od 11:00 do 14:00 hodin.
            </p>
        </div>
    );
};