import React, { useEffect } from 'react';
import { formatDistance } from 'date-fns';

export type AccommodationCardProps = {
    mainPhoto: string,
    location: string,
    dateAdded: Date,
    name: string,
    description: string,
}

export const AccommodationCard = ({ mainPhoto, location, dateAdded, name, description }: AccommodationCardProps) => {
    const dateAgo = formatDistance(new Date(dateAdded), new Date(), { addSuffix: true });

    return (
        <div className="ui fluid card" style={{maxWidth: "500px"}}>
            <div className="image">
                <img src={mainPhoto + '&fit=crop&w=500&h=500'} alt="Accommodation"/>
            </div>
            <div className="content">
                <div className="header">{name}</div>
                <div className="meta">
                    <span className="right floated time">{dateAgo}</span>
                    <span className="category">{location}</span>
                </div>
                <div className="description">
                    {description}
                </div>
            </div>
        </div>
    );
};
