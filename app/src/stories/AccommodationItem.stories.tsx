import React from 'react';
import { AccommodationItem } from './AccommodationItem';
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
    title: 'AirBnB/AccommodationItem',
    component: AccommodationItem,
} as ComponentMeta<typeof AccommodationItem>;

const Template: ComponentStory<typeof AccommodationItem> = (args) => <AccommodationItem {...args} />;

export const CasaTata = Template.bind({});
CasaTata.args = {
    name: "Casa Tata 4",
    description: "These gorgeous 9 cabins right in front of the sea are the perfect spot if you want to enjoy Chacahua’s captivating nature and still have a clean, quiet, luxurious and tasteful accommodation.",
    location: "Laguna de Chacahua, Oaxaca, Mexiko",
    mainPhoto: "https://images.unsplash.com/photo-1590725121839-892b458a74fe?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8",
};
