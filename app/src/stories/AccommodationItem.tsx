import React from 'react'

export type AccommodationItemProps = {
    name: string,
    description: string,
    location: string,
    mainPhoto: string,
}

export const AccommodationItem = ({ name, description, location, mainPhoto }: AccommodationItemProps) => {
    return (
        <div className="item">
            <div className="image">
                <img src={mainPhoto + '&fit=crop&w=200&h=200'} />
            </div>
            <div className="content">
                <div className="header">{name}</div>
                <div className="description">
                    <span>{description}</span>
                </div>
                <div className="extra">
                    {location}
                </div>
            </div>
        </div>
    )
}
