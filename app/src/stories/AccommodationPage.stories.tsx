import React from 'react';
import { Button } from 'react-native'
import { AccommodationPage } from './AccommodationPage';
import { ComponentMeta, ComponentStory } from '@storybook/react';

export default {
    title: 'AirBnB/Accommodation Page',
    component: AccommodationPage,
} as ComponentMeta<typeof AccommodationPage>;

const Template: ComponentStory<typeof AccommodationPage> = (args) => <AccommodationPage {...args} />;

const cardProps = {
    dateAdded: (new Date()),
    name: "Casa Tata 4",
    description: "These gorgeous 9 cabins right in front of the sea are the perfect spot if you want to enjoy Chacahua’s captivating nature and still have a clean, quiet, luxurious and tasteful accommodation.",
    location: "Laguna de Chacahua, Oaxaca, Mexiko",
    mainPhoto: "https://images.unsplash.com/photo-1590725121839-892b458a74fe?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8",
};

const reservationProps = {
    isDisabled: false,
}

export const Accommodation = Template.bind({});
Accommodation.args = {
    cardProps,
    reservationProps,
}
