import React, { useEffect } from 'react';
import { AccommodationCard, AccommodationCardProps } from './AccommodationCard';
import { Reservation, ReservationProps } from './Reservation';

export interface AccommodationPageProps {
    cardProps: AccommodationCardProps,
    reservationProps: ReservationProps,
}

export const AccommodationPage = ({ cardProps, reservationProps }: AccommodationPageProps) => {

    useEffect(() => {
        document.title = cardProps.name;
        console.log(document.title)
    });

    return (
        <div className="ui main container text app">
            <div className="accomodation-page ui two column grid">
                <div className="column nine wide">
                    <AccommodationCard {...cardProps} />
                </div>
                <div className="column seven wide">
                    <Reservation {...reservationProps} />
                </div>
            </div>
        </div>
    );
};
