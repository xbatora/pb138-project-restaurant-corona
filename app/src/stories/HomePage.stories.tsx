import React from 'react';
import { HomePage } from './HomePage';
import { ComponentMeta, ComponentStory } from '@storybook/react';

export default {
    title: 'AirBnB/Home Page',
    component: HomePage,
} as ComponentMeta<typeof HomePage>;


const Template: ComponentStory<typeof HomePage> = () => <HomePage />;

export const Page = Template.bind({});
