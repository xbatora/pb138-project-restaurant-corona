import React from 'react';
import { AccommodationItem, AccommodationItemProps } from './AccommodationItem';
import { photos } from '../data/restaurace'

export const HomePage = () => {
    return (
        <div className="ui main container text app">
            <div className="main image">
                <img src={photos[0].photoUrl + '&fit&w=358'} alt="Restaurace Korona"/>
            </div>
            <h1 className="ui header text-white">Restaurace Korona</h1>
            <p> Vážení hosté, připravili jsme pro Vás nabídku jídel do zázemí Vašich domovů. Nabízíme možnost si objednat u nás jídla s rozvozem či k vyzvednutí ke konzumaci v pohodlí vašich domovů či kanceláří.</p>
            <button className="ui submit button objednat obed" type="button">
                Objednat oběd
            </button>
            <h2>Týdenní menu</h2>
            <p>Každý týden máme pro Vás připravený výběr hlavních jídel.</p>
            <button className="ui submit button tydenni menu" type="button">
                Více
            </button>
            <div className="image">
                <img src={photos[1].photoUrl + '&fit&w=358'} alt="Jídlo"/>
            </div>
        </div>
    );
};
