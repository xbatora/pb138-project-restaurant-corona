import React from 'react';
import { Reservation } from './Reservation';
import { ComponentMeta, ComponentStory } from '@storybook/react';

export default {
    title: 'AirBnB/Reservation',
    component: Reservation,
} as ComponentMeta<typeof Reservation>;

const Template: ComponentStory<typeof Reservation> = (args) => <Reservation {...args} />;

export const Enabled = Template.bind({});
Enabled.args = {
    isDisabled: false,
}

export const Disabled = Template.bind({});
Disabled.args = {
    isDisabled: true,
}
