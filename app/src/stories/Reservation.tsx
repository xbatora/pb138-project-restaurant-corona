import React, { useState } from 'react';
import { DateRange, Range } from 'react-date-range';
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';

export interface ReservationProps {
    isDisabled: boolean;
}

export const Reservation = ({ isDisabled }: ReservationProps) => {
    const [ selection, setSelection ] = useState<Range>({
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection'
    });

    return (
        <form className={'ui form left floated'} onSubmit={(e) => {e.preventDefault()}}>
            <div className="field">
                <DateRange
                    ranges={[ selection ]}
                    onChange={({ selection }) => setSelection(selection)}
                />
            </div>
            <button className="ui submit button red fluid" type="submit" disabled={isDisabled}>
                <i className="paper plane icon"/>
                Make reservation
            </button>
        </form>
    );
};
