/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{vue,jsx,ts,js,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        beige: {
          DEFAULT:'#EEEAE1'
        }
      }
    },
  },
  plugins: [],

}
